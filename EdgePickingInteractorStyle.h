#include <vtkInteractorStyleTrackballCamera.h>

#include <vtkSmartPointer.h>

#include <vtkPolyDataAlgorithm.h>

#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkExtractSelection.h>
#include <vtkGeometryFilter.h>
#include <vtkExtractEdges.h>

#include <vtkActor.h>

#include <vtkAnnotationLink.h>
#include <vtkGraphAlgorithm.h>
#include <vtkConvertSelection.h>

#include <QtGui/QStatusBar>

class EdgePickingInteractorStyle : public vtkInteractorStyleTrackballCamera
{
  public:
    static EdgePickingInteractorStyle* New();
    vtkTypeMacro(EdgePickingInteractorStyle, vtkInteractorStyleTrackballCamera);
    void initialize(vtkPolyDataAlgorithm* polyDataWithCylinders, vtkRenderer* defaultRenderer, vtkGraphAlgorithm* graph, vtkAnnotationLink* activeEdgeSelectionLink, QStatusBar* statusBar);

    virtual void OnMouseMove();
    virtual void OnLeftButtonDown();
    virtual void OnLeftButtonUp();

  private:
    vtkSmartPointer<vtkSelectionNode> cylinderSelectionNode;
    vtkSmartPointer<vtkSelection> cylinderSelection;
    vtkSmartPointer<vtkExtractSelection> extractGlyphSelection;
    vtkSmartPointer<vtkGeometryFilter> cylinderSelectionGeometryFilter;

    bool hasClickStarted;
    bool isClick;
    void pick(int* coordinatesOfLastLeftClick);
    int coordinatesOfCurrentPosition[2];
    int coordinatesOfClickStart[2];

    vtkPolyDataAlgorithm* polyDataWithCylinders;

    vtkGraphAlgorithm* graph;

    vtkSmartPointer<vtkSelectionNode> edgeSelectionNode;
    vtkSmartPointer<vtkSelection> edgeSelection;
    vtkSmartPointer<vtkConvertSelection> convertEdgeSelectionTypeToIndices;

    vtkAnnotationLink* activeEdgeSelectionLink;

    QStatusBar* statusBar;
};
