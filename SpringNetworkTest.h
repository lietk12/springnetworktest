#ifndef SpringNetworkTest_H
# define SpringNetworkTest_H

# include <vtkSmartPointer.h>

# include <vtkIntArray.h>

# include <vtkPoints.h>
# include <vtkMutableUndirectedGraph.h>

# include <vtkDataObjectToTable.h>

# include <vtkViewUpdater.h>

# include <vtkGraphLayout.h>
# include <vtkPassThroughLayoutStrategy.h>
# include "RenderedGraphEdgesRepresentation.h"
# include "GraphEdgesLayoutView.h"

# include <vtkGraphToPolyData.h>
# include <vtkPolyDataMapper.h>
# include <vtkTubeFilter.h>

# include <vtkRenderWindow.h>
# include <vtkRenderer.h>
# include <vtkRenderWindowInteractor.h>
# include <vtkCamera.h>

# include "EdgePickingInteractorStyle.h"
# include <vtkInteractorStyleUnicam.h>

# include <vtkQtTableView.h>

# include <vtkAnnotationLink.h>
# include <vtkExtractSelectedGraph.h>
# include <vtkEdgeCenters.h>
# include <vtkSphereSource.h>
# include <vtkGlyph3DMapper.h>

# include "DataAnimation.h"

# include <QtGui/QMainWindow>
# include <QtGui/QStatusBar>

class Ui_SpringNetworkTest;

class SpringNetworkTest : public QMainWindow
{
  Q_OBJECT
public:
  SpringNetworkTest();
  ~SpringNetworkTest() {};
public slots:
  // File
  virtual void close();
  // Interaction
  virtual void setCylindersInteractorStyleToTrackballCamera();
  virtual void setCylindersInteractorStyleToUnicam();
  virtual void setCylindersInteractorStyleToEdgePicking();
  // Animation
  virtual void runStopAnimation();
  // Debug
  virtual void render();
  virtual void checkActiveSelection();
private:
  void initializeUi();
  Ui_SpringNetworkTest* ui;
  QStatusBar* statusBar;
  void initializeActions();

  // Renderers
  void initializeRenderWindows();
  vtkSmartPointer<vtkCamera> camera;
  void initializeCylindersRenderWindow();
  vtkSmartPointer<vtkRenderWindow> cylindersRenderWindow;
  vtkSmartPointer<vtkRenderWindowInteractor> cylindersRenderWindowInteractor;
  void initializeCylindersRenderer();
  vtkSmartPointer<vtkRenderer> cylindersRenderer;
  void initializeSkeletonRenderWindow();
  vtkSmartPointer<vtkRenderWindow> skeletonRenderWindow;
  vtkSmartPointer<vtkRenderWindowInteractor> skeletonRenderWindowInteractor;
  void initializeSkeletonRenderer();
  vtkSmartPointer<vtkRenderer> skeletonRenderer;

  // Graph data model
  void initializeGraph();
  vtkSmartPointer<vtkMutableUndirectedGraph> g;
  void addVertex(vtkSmartPointer<vtkIntArray> vertexIDs,
                 vtkSmartPointer<vtkPoints> points, double x, double y, double z);
  void addEdge(vtkSmartPointer<vtkIntArray> edgeIDs,
               vtkIdType vertex1, vtkIdType vertex2,
               vtkSmartPointer<vtkIntArray> generations, int generation,
               vtkSmartPointer<vtkDoubleArray> weights, double weight);

  // Graph filters
  void initializeGraphLayout();
  vtkSmartPointer<vtkPassThroughLayoutStrategy> graphLayoutStrategy;
  vtkSmartPointer<vtkGraphLayout> graphLayout;
  void initializeDataAnimator();
  vtkSmartPointer<vtkProgrammableFilter> programmableFilter;
  vtkSmartPointer<DataAnimation> dataAnimator;

  // Graph views
  void initializeGraphViews();
  vtkSmartPointer<vtkViewUpdater> graphViewUpdater;
  void initializeCylindersView();
  vtkSmartPointer<vtkGraphToPolyData> graphToPolyData;
  vtkSmartPointer<vtkTubeFilter> polyDataToCylinders;
  vtkSmartPointer<vtkPolyDataMapper> cylindersMapper;
  vtkSmartPointer<vtkActor> cylindersActor;
  vtkSmartPointer<vtkRenderView> cylindersView;
  void initializeSkeletonView();
  vtkDataRepresentation* skeletonRepresentation;
  vtkSmartPointer<GraphEdgesLayoutView> skeletonView;
  void initializeEdgesTableView();
  vtkDataRepresentation* edgesTableRepresentation;
  vtkSmartPointer<vtkQtTableView> edgesTableView;

  // Interaction
  void initializeInteractorStyles();
  void initializeTrackballCameraInteractorStyle();
  void initializeUnicamInteractorStyle();
  void initializeEdgePickingInteractorStyle();
  void initializeCylindersRenderWindowInteractor();
  enum InteractorStyle {TRACKBALL_CAMERA, UNICAM, EDGE_PICKING};
  void setActiveCylindersInteractorStyle(InteractorStyle interactorStyle);
  InteractorStyle activeInteractorStyleType;
  vtkSmartPointer<vtkInteractorStyle> activeCylindersInteractorStyle; // initializeInteractorStyles
  vtkSmartPointer<vtkInteractorStyleTrackballCamera> trackballCameraInteractorStyle; // initializeTrackballCameraInteractorStyle
  vtkSmartPointer<vtkInteractorStyleUnicam> unicamInteractorStyle; // initializeUnicamInteractorStyle
  vtkSmartPointer<EdgePickingInteractorStyle> edgePickingInteractorStyle; // initializeEdgePickingInteractorStyle

  // Selection
  void initializeActiveSelection();
  vtkSmartPointer<vtkAnnotationLink> activeEdgeSelectionLink;
  vtkSmartPointer<vtkExtractSelectedGraph> selectedSubGraphExtraction;
  vtkSmartPointer<vtkEdgeCenters> selectedSubGraphToPolyData;
  vtkSmartPointer<vtkSphereSource> selectedEdgeIndicator;
  vtkSmartPointer<vtkGlyph3DMapper> selectedSubPolyDataMapper;
  vtkSmartPointer<vtkActor> selectedSubPolyDataActor;
  void initializeActiveSelectionTableView();
  vtkDataRepresentation* activeSelectionTableRepresentation;
  vtkSmartPointer<vtkQtTableView> activeSelectionTableView;

  // Animation
  bool isAnimating; // initializeRenderWindowInteractor
  int dataAnimationTimerId; // initializeRenderWindowInteractor
  void runAnimation();
  void stopAnimation();
};

#endif                                                      // SpringNetworkTest_H
