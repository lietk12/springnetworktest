#include <QtGui/QApplication>
#include "SpringNetworkTest.h"


int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    
    SpringNetworkTest springNetworkTest;
    springNetworkTest.show();
    
    return app.exec();
}
