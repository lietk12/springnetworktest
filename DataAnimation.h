#include <vtkCommand.h>
#include <vtkObjectFactory.h>
#include <vtkSmartPointer.h>
#include <vtkProgrammableFilter.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkQtTableView.h>

class DataAnimation : public vtkCommand
{
public:
  vtkTypeMacro(DataAnimation, vtkCommand);
  static DataAnimation *New();

  vtkSmartPointer<vtkProgrammableFilter> programmableFilter;

  void Execute(vtkObject *caller, unsigned long vtkNotUsed(eventId), void *vtkNotUsed(callData));

  vtkRenderWindowInteractor* cylindersRenderWindowInteractor;
  vtkRenderWindowInteractor* skeletonRenderWindowInteractor;
  vtkQtTableView* edgesTableView;
  vtkQtTableView* activeSelectionTableView;

  static unsigned int counter;
  static void updateVertexPositions(void* arguments);
};
