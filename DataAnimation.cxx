#include "DataAnimation.h"
#include <iostream>

#include <vtkGraph.h>
#include <vtkDataSetAttributes.h>
#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkInEdgeIterator.h>

#include <cmath>
#define _USE_MATH_DEFINES

DataAnimation* DataAnimation::New()
{
  return new DataAnimation;
}

// TODO: convert to smart pointers where appropriate
void DataAnimation::Execute(vtkObject *caller, unsigned long vtkNotUsed(eventId), void *vtkNotUsed(callData))
{
  this->programmableFilter->Modified();
  this->cylindersRenderWindowInteractor->Render();
  this->skeletonRenderWindowInteractor->Render();
  this->edgesTableView->Update();
  this->activeSelectionTableView->Update();
}

unsigned int DataAnimation::counter;

// TODO: convert to smart pointers where appropriate
void DataAnimation::updateVertexPositions(void* arguments)
{
  vtkProgrammableFilter* filterData = static_cast<vtkProgrammableFilter*>(arguments);

  // Get the graph and important data
  vtkSmartPointer<vtkGraph> inGraph = filterData->GetGraphInput();
  vtkSmartPointer<vtkDataSetAttributes> edgeData = inGraph->GetEdgeData();
  vtkSmartPointer<vtkDoubleArray> lengths = vtkDoubleArray::SafeDownCast(edgeData->GetArray("Lengths"));
  vtkSmartPointer<vtkDoubleArray> directions = vtkDoubleArray::SafeDownCast(edgeData->GetArray("Directions"));
  vtkSmartPointer<vtkDoubleArray> midpoints = vtkDoubleArray::SafeDownCast(edgeData->GetArray("Midpoints"));
  vtkPoints* points = inGraph->GetPoints();

  // Find the point of interest
  vtkIdType vertexOfInterest = inGraph->FindVertex(10);
  double* point = points->GetPoint(vertexOfInterest);

  // Move the point of interest
  point[0] += 0.05 * sin(M_PI * 0.1 * counter);
  point[1] += 0.05 * cos(M_PI * 0.1 * counter);
  point[2] += 0.05 * sin(M_PI * 0.1 * counter);
  points->SetPoint(vertexOfInterest, point);
  counter++;

  // Update all edges attached to the point of interest
  vtkSmartPointer<vtkInEdgeIterator> inEdgeIterator = vtkSmartPointer<vtkInEdgeIterator>::New();
  filterData->GetGraphOutput()->GetInEdges(vertexOfInterest, inEdgeIterator);
  vtkInEdgeType currentEdge;
  double sourcePoint[3];
  double direction[3];
  double midpoint[3];
  double currentEdgeLength;
  while (inEdgeIterator->HasNext()) {
    currentEdge = inEdgeIterator->Next();
    points->GetPoint(currentEdge.Source, sourcePoint);
    for (int i = 0; i < 3; i++) {
      direction[i] = sourcePoint[i] - point[i];
      midpoint[i] = (sourcePoint[i] + point[i]) / 2;
    }
    currentEdgeLength = sqrt(direction[0] * direction[0] + direction[1] * direction[1] + direction[2] * direction[2]);
    directions->SetTupleValue(currentEdge.Id, direction);
    lengths->SetValue(currentEdge.Id, currentEdgeLength);
    midpoints->SetTupleValue(currentEdge.Id, midpoint);
  }

  // Update output with the new points
    filterData->GetGraphOutput()->DeepCopy(filterData->GetGraphInput());
    filterData->GetGraphOutput()->SetPoints(points);
}
