#include "EdgePickingInteractorStyle.h"
#include <vtkObjectFactory.h>

#include <vtkCellPicker.h>

#include <vtkIdTypeArray.h>
#include <vtkIntArray.h>
#include <vtkCellData.h>
#include <vtkCell.h>

#include <vtkProperty.h>

#include <vtkUnstructuredGrid.h>

#include <vtkRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>

#include <qstring.h>

vtkStandardNewMacro(EdgePickingInteractorStyle);

void EdgePickingInteractorStyle::initialize(vtkPolyDataAlgorithm* polyDataWithCylinders, vtkRenderer* defaultRenderer, vtkGraphAlgorithm* graph, vtkAnnotationLink* activeEdgeSelectionLink, QStatusBar* statusBar)
{
  this->polyDataWithCylinders = polyDataWithCylinders;

  this->cylinderSelectionNode = vtkSmartPointer<vtkSelectionNode>::New();
  this->cylinderSelectionNode->SetFieldType(vtkSelectionNode::CELL);
  this->cylinderSelectionNode->SetContentType(vtkSelectionNode::INDICES);

  this->cylinderSelection = vtkSmartPointer<vtkSelection>::New();
  this->cylinderSelection->AddNode(this->cylinderSelectionNode);

  this->extractGlyphSelection = vtkSmartPointer<vtkExtractSelection>::New();
  this->extractGlyphSelection->SetInputConnection(0, this->polyDataWithCylinders->GetOutputPort(0));
  this->extractGlyphSelection->SetInputData(1, this->cylinderSelection);

  this->cylinderSelectionGeometryFilter = vtkSmartPointer<vtkGeometryFilter>::New();
  this->cylinderSelectionGeometryFilter->SetInputConnection(extractGlyphSelection->GetOutputPort(0));

  this->edgeSelectionNode = vtkSmartPointer<vtkSelectionNode>::New();
  this->edgeSelectionNode->SetFieldType(vtkSelectionNode::EDGE);
  this->edgeSelectionNode->SetContentType(vtkSelectionNode::VALUES);

  this->edgeSelection = vtkSmartPointer<vtkSelection>::New();
  this->edgeSelection->AddNode(this->edgeSelectionNode);

  this->graph = graph;

  this->convertEdgeSelectionTypeToIndices = vtkSmartPointer<vtkConvertSelection>::New();
  this->convertEdgeSelectionTypeToIndices->SetInputData(this->edgeSelection);
  this->convertEdgeSelectionTypeToIndices->SetDataObjectConnection(this->graph->GetOutputPort(0));
  this->convertEdgeSelectionTypeToIndices->SetOutputType(vtkSelectionNode::INDICES);
  this->convertEdgeSelectionTypeToIndices->SetArrayName("EdgeIDs");

  this->SetDefaultRenderer(defaultRenderer);

  this->activeEdgeSelectionLink = activeEdgeSelectionLink;

  this->statusBar = statusBar;
}

void EdgePickingInteractorStyle::OnMouseMove()
{
  if (this->hasClickStarted) {
    this->coordinatesOfCurrentPosition[0] = this->GetInteractor()->GetEventPosition()[0];
    this->coordinatesOfCurrentPosition[1] = this->GetInteractor()->GetEventPosition()[1];
    if ((abs(this->coordinatesOfCurrentPosition[0] - this->coordinatesOfClickStart[0]) > 2) ||
      (abs(this->coordinatesOfCurrentPosition[1] - this->coordinatesOfClickStart[1]) > 2)) {
      this->isClick = false;
    }
  }
  vtkInteractorStyleTrackballCamera::OnMouseMove();
}

void EdgePickingInteractorStyle::OnLeftButtonDown()
{
  if (!this->hasClickStarted) {
    this->hasClickStarted = true;
    this->isClick = true;
    this->coordinatesOfClickStart[0] = this->GetInteractor()->GetEventPosition()[0];
    this->coordinatesOfClickStart[1] = this->GetInteractor()->GetEventPosition()[1];
  }
  vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
}

void EdgePickingInteractorStyle::OnLeftButtonUp()
{
  this->hasClickStarted = false;
  if (this->isClick) {
    this->pick(this->GetInteractor()->GetEventPosition());
  }
  vtkInteractorStyleTrackballCamera::OnLeftButtonUp();
}


void EdgePickingInteractorStyle::pick(int* coordinatesOfLastLeftClick)
{
  vtkSmartPointer<vtkCellPicker> cellPicker = vtkSmartPointer<vtkCellPicker>::New();
  // TODO: avoid magic numbers
  cellPicker->SetTolerance(0.005);
  cellPicker->Pick(coordinatesOfLastLeftClick[0], coordinatesOfLastLeftClick[1], 0, this->GetDefaultRenderer());

  if (cellPicker->GetCellId() == -1) {
    vtkSmartPointer<vtkIdTypeArray> ids = vtkSmartPointer<vtkIdTypeArray>::New();
    ids->SetNumberOfComponents(1);
    this->cylinderSelectionNode->SetSelectionList(ids);

    this->edgeSelectionNode->GetSelectionList()->Reset();

    this->statusBar->showMessage(QString("Clicked on no edges."));
  } else {
    vtkSmartPointer<vtkIdTypeArray> ids = vtkSmartPointer<vtkIdTypeArray>::New();
    ids->SetNumberOfComponents(1);
    ids->InsertNextValue(cellPicker->GetCellId());
    this->cylinderSelectionNode->SetSelectionList(ids);
    this->cylinderSelectionNode->Modified();

    this->extractGlyphSelection->Update();
    this->cylinderSelectionGeometryFilter->Update();

    int edgeID = vtkIntArray::SafeDownCast(this->cylinderSelectionGeometryFilter->GetOutput()->GetCellData()->GetArray("EdgeIDs"))->GetValue(0);

    this->edgeSelectionNode->SetSelectionList(vtkIntArray::SafeDownCast(this->cylinderSelectionGeometryFilter->GetOutput()->GetCellData()->GetArray("EdgeIDs")));

    this->statusBar->showMessage(QString("Clicked on edge %1").arg(edgeID));
  }

  this->edgeSelectionNode->Modified();
  this->convertEdgeSelectionTypeToIndices->Update();
  this->activeEdgeSelectionLink->SetCurrentSelection(this->convertEdgeSelectionTypeToIndices->GetOutput());
}
