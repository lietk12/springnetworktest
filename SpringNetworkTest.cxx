#include "SpringNetworkTest.h"
#include "ui_SpringNetworkTest.h"

#include <iostream>
#include <sstream>

#include <QtGui/QLabel>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QAction>
#include <QtGui/QActionGroup>
#include <QtGui/QTableView>

#include <GL/gl.h>

#include <vtkIntArray.h>
#include <vtkDoubleArray.h>
#include <vtkDataSetAttributes.h>
#include <vtkDataRepresentation.h>

#include <vtkEdgeListIterator.h>

#include <vtkCubeAxesActor.h>

#include <vtkProperty.h>

SpringNetworkTest::SpringNetworkTest()
{
  initializeUi();
  initializeActions();

  initializeRenderWindows();
  this->cylindersRenderWindow->Render();
  glEnable(GL_FOG);
  glFogf(GL_FOG_MODE, GL_EXP2);
  glFogf(GL_FOG_DENSITY, 0.28);
  GLfloat fogColor[4] = {0.8, 0.8, 0.8, 1.0};
  glFogfv(GL_FOG_COLOR, fogColor);

  initializeGraph();
  initializeGraphViews();

  initializeCylindersRenderWindowInteractor();
  runAnimation();

  this->statusBar->showMessage(tr("Ready"));
}

void SpringNetworkTest::initializeUi()
{
  this->ui = new Ui_SpringNetworkTest;
  this->ui->setupUi(this);
  this->statusBar = this->ui->statusBar;

  QActionGroup* interactorStyles = new QActionGroup(this);
  ui->actionTrackballCamera->setActionGroup(interactorStyles);
  ui->actionUnicam->setActionGroup(interactorStyles);
  ui->actionBlockSelection->setActionGroup(interactorStyles);
  ui->actionEdgePicking->setActionGroup(interactorStyles);

  this->tabifyDockWidget(this->ui->dockWidgetEdgesTableView, this->ui->dockWidgetSkeletonView);
  this->tabifyDockWidget(this->ui->dockWidgetSkeletonView, this->ui->dockWidgetCylindersView);
}

void SpringNetworkTest::initializeActions()
{
  // File
  connect(this->ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));

  // Interaction
  connect(this->ui->actionTrackballCamera, SIGNAL(triggered()), this, SLOT(setCylindersInteractorStyleToTrackballCamera()));
  connect(this->ui->actionUnicam, SIGNAL(triggered()), this, SLOT(setCylindersInteractorStyleToUnicam()));
  connect(this->ui->actionEdgePicking, SIGNAL(triggered()), this, SLOT(setCylindersInteractorStyleToEdgePicking()));

  // Animation
  connect(this->ui->actionRunStopAnimation, SIGNAL(triggered()), this, SLOT(runStopAnimation()));

  // Debug
  connect(this->ui->actionRender, SIGNAL(triggered()), this, SLOT(render()));
  connect(this->ui->actionCheckActiveSelection, SIGNAL(triggered()), this, SLOT(checkActiveSelection()));
}

void SpringNetworkTest::initializeRenderWindows()
{
  this->camera = vtkSmartPointer<vtkCamera>::New();
  initializeCylindersRenderWindow();
  initializeSkeletonRenderWindow();
}

void SpringNetworkTest::initializeCylindersRenderWindow()
{
  this->cylindersRenderWindow = this->ui->cylindersView->GetRenderWindow();
  initializeCylindersRenderer();
  this->cylindersRenderWindow->AddRenderer(this->cylindersRenderer);
  this->cylindersRenderWindowInteractor = this->ui->cylindersView->GetInteractor();
  this->cylindersRenderWindowInteractor->SetRenderWindow(this->cylindersRenderWindow);
}

void SpringNetworkTest::initializeCylindersRenderer()
{
  this->cylindersRenderer = vtkSmartPointer<vtkRenderer>::New();
  this->cylindersRenderer->SetBackground(0.8, 0.8, 0.8);
  this->cylindersRenderer->SetActiveCamera(this->camera);
}

void SpringNetworkTest::initializeSkeletonRenderWindow()
{
  this->skeletonRenderWindow = this->ui->skeletonView->GetRenderWindow();
  initializeSkeletonRenderer();
  this->skeletonRenderWindow->AddRenderer(this->skeletonRenderer);
  this->skeletonRenderWindowInteractor = this->ui->skeletonView->GetInteractor();
  this->skeletonRenderWindowInteractor->SetRenderWindow(this->skeletonRenderWindow);
}

void SpringNetworkTest::initializeSkeletonRenderer()
{
  this->skeletonRenderer = vtkSmartPointer<vtkRenderer>::New();
  this->skeletonRenderer->SetBackground(0.8, 0.8, 0.8);
  this->skeletonRenderer->SetActiveCamera(this->camera);
}

void SpringNetworkTest::initializeGraph()
{
  this->g = vtkSmartPointer<vtkMutableUndirectedGraph>::New();
  // Add vertices
  this->g->GetVertexData()->SetPedigreeIds(vtkSmartPointer<vtkIntArray>::New());
  // Set up vertex ids array
  vtkSmartPointer<vtkIntArray> vertexIDs = vtkSmartPointer<vtkIntArray>::New();
  vertexIDs->SetNumberOfComponents(1);
  vertexIDs->SetName("VertexIDs");
  // Set up vertex points array
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  this->g->SetPoints(points);

  addVertex(vertexIDs, points, 0.0, 0.0, 0.0);
  addVertex(vertexIDs, points, 0.3, 0.0, 0.0);
  addVertex(vertexIDs, points, 0.0, 0.3, 0.0);
  addVertex(vertexIDs, points, 0.3, 0.3, 0.1);
  addVertex(vertexIDs, points, 0.6, 0.2, 0.4);
  addVertex(vertexIDs, points, 0.2, 0.6, 0.4);
  addVertex(vertexIDs, points, 0.5, 0.5, 0.4);
  addVertex(vertexIDs, points, 1.0, 0.8, 0.8);
  addVertex(vertexIDs, points, 0.8, 1.0, 0.8);
  addVertex(vertexIDs, points, 0.7, 0.7, 0.8);
  addVertex(vertexIDs, points, 1.0, 1.0, 1.0);
  addVertex(vertexIDs, points, 0.0, 1.0, 0.0);
  addVertex(vertexIDs, points, 0.0, 0.0, 1.0);
  addVertex(vertexIDs, points, 0.0, 1.0, 1.0);
  addVertex(vertexIDs, points, 1.0, 0.0, 1.0);
  addVertex(vertexIDs, points, 1.0, 1.0, 0.0);
  addVertex(vertexIDs, points, 1.0, 0.0, 0.0);
  // Add the property arrays
  this->g->GetVertexData()->AddArray(vertexIDs);

  // Add edges
  // Set up edge ids array
  vtkSmartPointer<vtkIntArray> edgeIDs = vtkSmartPointer<vtkIntArray>::New();
  edgeIDs->SetNumberOfComponents(1);
  edgeIDs->SetName("EdgeIDs");
  // Set up edge generations array
  vtkSmartPointer<vtkIntArray> edgeGenerations = vtkSmartPointer<vtkIntArray>::New();
  edgeGenerations->SetNumberOfComponents(1);
  edgeGenerations->SetName("Generations");
  // Set up edge weights array
  vtkSmartPointer<vtkDoubleArray> weights = vtkSmartPointer<vtkDoubleArray>::New();
  weights->SetNumberOfComponents(1);
  weights->SetName("Weights");

  addEdge(edgeIDs, 0, 3, edgeGenerations, 0, weights, 0.1);
  addEdge(edgeIDs, 0, 1, edgeGenerations, 0, weights, 0.05);
  addEdge(edgeIDs, 0, 2, edgeGenerations, 0, weights, 0.05);
  addEdge(edgeIDs, 1, 4, edgeGenerations, 0, weights, 0.1);
  addEdge(edgeIDs, 1, 3, edgeGenerations, 1, weights, 0.05);
  addEdge(edgeIDs, 2, 3, edgeGenerations, 1, weights, 0.05);
  addEdge(edgeIDs, 2, 5, edgeGenerations, 1, weights, 0.1);
  addEdge(edgeIDs, 4, 6, edgeGenerations, 1, weights, 0.05);
  addEdge(edgeIDs, 5, 6, edgeGenerations, 1, weights, 0.05);
  addEdge(edgeIDs, 3, 6, edgeGenerations, 2, weights, 0.1);
  addEdge(edgeIDs, 7, 9, edgeGenerations, 2, weights, 0.05);
  addEdge(edgeIDs, 8, 9, edgeGenerations, 2, weights, 0.05);
  addEdge(edgeIDs, 4, 7, edgeGenerations, 2, weights, 0.1);
  addEdge(edgeIDs, 5, 8, edgeGenerations, 3, weights, 0.1);
  addEdge(edgeIDs, 6, 9, edgeGenerations, 3, weights, 0.1);
  addEdge(edgeIDs, 7, 10, edgeGenerations, 3, weights, 0.05);
  addEdge(edgeIDs, 8, 10, edgeGenerations, 3, weights, 0.05);
  addEdge(edgeIDs, 9, 10, edgeGenerations, 3, weights, 0.1);
  // Add the property arrays
  this->g->GetEdgeData()->AddArray(edgeIDs);
  this->g->GetEdgeData()->AddArray(edgeGenerations);
  this->g->GetEdgeData()->AddArray(weights);
  this->g->GetEdgeData()->SetScalars(weights);

  // Set up edge lengths and directions arrays
  vtkSmartPointer<vtkDoubleArray> lengths = vtkSmartPointer<vtkDoubleArray>::New();
  lengths->SetNumberOfComponents(1);
  lengths->SetName("Lengths");
  lengths->SetNumberOfValues(this->g->GetNumberOfEdges());
  vtkSmartPointer<vtkDoubleArray> directions = vtkSmartPointer<vtkDoubleArray>::New();
  directions->SetNumberOfComponents(3);
  directions->SetName("Directions");
  directions->SetNumberOfTuples(this->g->GetNumberOfEdges());
  vtkSmartPointer<vtkDoubleArray> midpoints = vtkSmartPointer<vtkDoubleArray>::New();
  midpoints->SetNumberOfComponents(3);
  midpoints->SetName("Midpoints");
  midpoints->SetNumberOfTuples(this->g->GetNumberOfEdges());
  // Iterate through the edges
  vtkSmartPointer<vtkEdgeListIterator> edgeListIterator = vtkSmartPointer<vtkEdgeListIterator>::New();
  this->g->GetEdges(edgeListIterator);
  vtkEdgeType currentEdge;
  double sourcePoint[3];
  double targetPoint[3];
  double direction[3];
  double midpoint[3];
  double currentEdgeLength;
  while (edgeListIterator->HasNext()) {
    currentEdge = edgeListIterator->Next();
    points->GetPoint(currentEdge.Source, sourcePoint);
    points->GetPoint(currentEdge.Target, targetPoint);
    for (int i = 0; i < 3; i++) {
      direction[i] = sourcePoint[i] - targetPoint[i];
      midpoint[i] = (sourcePoint[i] + targetPoint[i]) / 2;
    }
    currentEdgeLength = sqrt(direction[0] * direction[0] + direction[1] * direction[1] + direction[2] * direction[2]);
    directions->SetTupleValue(currentEdge.Id, direction);
    lengths->SetValue(currentEdge.Id, currentEdgeLength);
    midpoints->SetTupleValue(currentEdge.Id, midpoint);
  }
  // Add the property array
  this->g->GetEdgeData()->AddArray(lengths);
  this->g->GetEdgeData()->AddArray(directions);
  this->g->GetEdgeData()->AddArray(midpoints);
}

void SpringNetworkTest::addVertex(vtkSmartPointer<vtkIntArray> vertexIDs,
                                  vtkSmartPointer<vtkPoints> points, double x, double y, double z)
{
  this->g->AddVertex(this->g->GetNumberOfVertices());
  vertexIDs->InsertNextValue(this->g->GetNumberOfVertices());
  points->InsertNextPoint(x, y, z);
}

void SpringNetworkTest::addEdge(vtkSmartPointer<vtkIntArray> edgeIDs,
                                vtkIdType vertex1, vtkIdType vertex2,
                                vtkSmartPointer<vtkIntArray> generations, int generation,
                                vtkSmartPointer<vtkDoubleArray> weights, double weight)
{
  edgeIDs->InsertNextValue(this->g->GetNumberOfEdges());
  this->g->AddEdge(vertex1, vertex2);
  generations->InsertNextValue(generation);
  weights->InsertNextValue(weight);
}

void SpringNetworkTest::initializeGraphViews()
{
  initializeDataAnimator();
  initializeGraphLayout();

  this->graphViewUpdater = vtkViewUpdater::New();
  initializeEdgesTableView();
  this->graphViewUpdater->AddView(this->edgesTableView);
  initializeSkeletonView();
  this->graphViewUpdater->AddView(this->skeletonView);
  initializeCylindersView();

  initializeActiveSelection();
  initializeActiveSelectionTableView();
  initializeInteractorStyles();
  setActiveCylindersInteractorStyle(EDGE_PICKING);
}

// TODO: make things into member variables
void SpringNetworkTest::initializeCylindersView()
{
  this->graphToPolyData = vtkSmartPointer<vtkGraphToPolyData>::New();
  this->graphToPolyData->SetInputConnection(this->graphLayout->GetOutputPort(0));

  // TODO: make these magic numbers be user settings
  this->polyDataToCylinders = vtkSmartPointer<vtkTubeFilter>::New();
  this->polyDataToCylinders->SetRadius(0.02);
  this->polyDataToCylinders->SetVaryRadiusToVaryRadiusOff();
  this->polyDataToCylinders->SetNumberOfSides(12);
  this->polyDataToCylinders->CappingOn();
  this->polyDataToCylinders->SidesShareVerticesOn();
  this->polyDataToCylinders->SetInputConnection(this->graphToPolyData->GetOutputPort(0));

  this->cylindersMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  this->cylindersMapper->SetInputConnection(this->polyDataToCylinders->GetOutputPort(0));
  this->cylindersMapper->ImmediateModeRenderingOn();

  this->cylindersActor = vtkSmartPointer<vtkActor>::New();
  this->cylindersActor->SetMapper(cylindersMapper);

  this->cylindersView = vtkSmartPointer<vtkRenderView>::New();
  this->cylindersView->SetRenderWindow(this->cylindersRenderWindow);
  this->cylindersView->SetRenderer(this->cylindersRenderer);
  this->cylindersView->SetInteractor(this->cylindersRenderWindowInteractor);

  this->cylindersRenderer->AddActor(this->cylindersActor);

  // Make axes actor
  vtkSmartPointer<vtkCubeAxesActor> cubeAxesActor = vtkSmartPointer<vtkCubeAxesActor>::New();
  // TODO: update bounds with animation
  cubeAxesActor->SetBounds(this->cylindersMapper->GetBounds());
  cubeAxesActor->SetCamera(this->camera);
  cubeAxesActor->PickableOff();
  this->cylindersRenderer->AddActor(cubeAxesActor);

  this->cylindersView->ResetCamera();
}

void SpringNetworkTest::initializeDataAnimator()
{
  // TODO: move this stuff into initializer method
  this->programmableFilter = vtkSmartPointer<vtkProgrammableFilter>::New();
  this->programmableFilter->SetInputData(0, this->g);
  this->programmableFilter->SetExecuteMethod(DataAnimation::updateVertexPositions, programmableFilter);

  this->dataAnimator = vtkSmartPointer<DataAnimation>::New();
  this->dataAnimator->programmableFilter = this->programmableFilter;
  this->dataAnimator->cylindersRenderWindowInteractor = this->cylindersRenderWindowInteractor;
  this->dataAnimator->skeletonRenderWindowInteractor = this->skeletonRenderWindowInteractor;
  // FIXME: this doesn't actually do anything for the dataAnimator member
  this->dataAnimator->edgesTableView = this->edgesTableView;
}

void SpringNetworkTest::initializeInteractorStyles()
{
  initializeTrackballCameraInteractorStyle();
  initializeUnicamInteractorStyle();
  initializeEdgePickingInteractorStyle();
}

void SpringNetworkTest::initializeTrackballCameraInteractorStyle()
{
  this->trackballCameraInteractorStyle = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
}

void SpringNetworkTest::initializeUnicamInteractorStyle()
{
  this->unicamInteractorStyle = vtkSmartPointer<vtkInteractorStyleUnicam>::New();
}

void SpringNetworkTest::initializeEdgePickingInteractorStyle()
{
  this->edgePickingInteractorStyle = vtkSmartPointer<EdgePickingInteractorStyle>::New();
  this->edgePickingInteractorStyle->initialize(this->polyDataToCylinders, this->cylindersRenderer, this->graphLayout, this->activeEdgeSelectionLink, this->statusBar);
}

void SpringNetworkTest::initializeGraphLayout()
{
  this->graphLayout = vtkSmartPointer<vtkGraphLayout>::New();
  this->graphLayout->SetInputConnection(this->programmableFilter->GetOutputPort(0));
  this->graphLayoutStrategy = vtkSmartPointer<vtkPassThroughLayoutStrategy>::New();
  this->graphLayout->SetLayoutStrategy(graphLayoutStrategy);
}

void SpringNetworkTest::initializeCylindersRenderWindowInteractor()
{
  this->cylindersRenderWindowInteractor->Initialize();
  this->cylindersRenderWindowInteractor->AddObserver(vtkCommand::TimerEvent, this->dataAnimator);
}

void SpringNetworkTest::setActiveCylindersInteractorStyle(SpringNetworkTest::InteractorStyle interactorStyle)
{
  this->activeInteractorStyleType = interactorStyle;
  switch (interactorStyle) {
    case TRACKBALL_CAMERA:
      this->activeCylindersInteractorStyle = this->trackballCameraInteractorStyle;
      this->ui->actionTrackballCamera->setChecked(true);
      break;
    case UNICAM:
      this->activeCylindersInteractorStyle = this->unicamInteractorStyle;
      this->ui->actionUnicam->setChecked(true);
      break;
    case  EDGE_PICKING:
      this->activeCylindersInteractorStyle = this->edgePickingInteractorStyle;
      this->ui->actionEdgePicking->setChecked(true);
      break;
  }

  this->cylindersView->SetInteractorStyle(this->activeCylindersInteractorStyle);
  this->cylindersRenderWindowInteractor->SetInteractorStyle(this->activeCylindersInteractorStyle);
}

void SpringNetworkTest::initializeEdgesTableView()
{
  this->edgesTableView = vtkSmartPointer<vtkQtTableView>::New();
  this->edgesTableView->SetFieldType(vtkQtTableView::EDGE_DATA);
  this->edgesTableView->SetSelectionBehavior(vtkQtTableView::SELECT_ROWS);
  this->edgesTableView->SetShowHorizontalHeaders(true);
  this->edgesTableView->SetShowVerticalHeaders(false);
  this->edgesTableView->SetEnableDragDrop(false);
  this->edgesTableView->SetSortingEnabled(true);
  // FIXME: selections don't actually get sorted to the top
  this->edgesTableView->SetSortSelectionToTop(true);
  this->edgesTableView->SetSplitMultiComponentColumns(false);

  this->edgesTableView->SetRepresentationFromInputConnection(this->graphLayout->GetOutputPort(0));
  this->edgesTableRepresentation = this->edgesTableView->GetRepresentation(0);
  this->activeEdgeSelectionLink = this->edgesTableRepresentation->GetAnnotationLink();
  this->graphViewUpdater->AddAnnotationLink(this->activeEdgeSelectionLink);

  this->dataAnimator->edgesTableView = this->edgesTableView;

  this->ui->edgesTableView->layout()->addWidget(this->edgesTableView->GetWidget());
  this->edgesTableView->Update();

}

void SpringNetworkTest::initializeSkeletonView()
{
  this->skeletonView = vtkSmartPointer<GraphEdgesLayoutView>::New();
  this->skeletonView->SetRenderWindow(this->skeletonRenderWindow);
  this->skeletonView->SetRenderer(this->skeletonRenderer);
  this->skeletonView->SetInteractor(this->skeletonRenderWindowInteractor);
  this->skeletonView->SetLayoutStrategy(this->graphLayoutStrategy);
  this->skeletonView->SetVertexVisibility(false);

  this->skeletonView->SetRepresentationFromInputConnection(this->graphLayout->GetOutputPort(0));
  this->skeletonRepresentation = this->skeletonView->GetRepresentation(0);
  this->skeletonRepresentation->SetAnnotationLink(this->activeEdgeSelectionLink);
  this->skeletonView->ResetCamera();
}

// Selection
void SpringNetworkTest::initializeActiveSelection()
{
  this->selectedSubGraphExtraction = vtkSmartPointer<vtkExtractSelectedGraph>::New();
  this->selectedSubGraphExtraction->SetInputConnection(this->graphLayout->GetOutputPort(0));
  this->selectedSubGraphExtraction->SetSelectionConnection(this->activeEdgeSelectionLink->GetOutputPort(2));

  this->selectedSubGraphToPolyData = vtkSmartPointer<vtkEdgeCenters>::New();
  this->selectedSubGraphToPolyData->SetInputConnection(this->selectedSubGraphExtraction->GetOutputPort(0));
  this->selectedSubGraphToPolyData->VertexCellsOff();

  // TODO: make magic numbers (also resolution) be user settings
  this->selectedEdgeIndicator = vtkSmartPointer<vtkSphereSource>::New();
  this->selectedEdgeIndicator->SetRadius(0.04);

  this->selectedSubPolyDataMapper = vtkSmartPointer<vtkGlyph3DMapper>::New();
  this->selectedSubPolyDataMapper->SetInputConnection(this->selectedSubGraphToPolyData->GetOutputPort(0));
  this->selectedSubPolyDataMapper->SetSourceConnection(this->selectedEdgeIndicator->GetOutputPort(0));
  this->selectedSubPolyDataMapper->ScalingOff();
  this->selectedSubPolyDataMapper->OrientOff();

  this->selectedSubPolyDataActor = vtkSmartPointer<vtkActor>::New();
  this->selectedSubPolyDataActor->SetMapper(this->selectedSubPolyDataMapper);
  this->selectedSubPolyDataActor->PickableOff();

  this->cylindersRenderer->AddActor(this->selectedSubPolyDataActor);
}

void SpringNetworkTest::initializeActiveSelectionTableView()
{
  this->activeSelectionTableView = vtkSmartPointer<vtkQtTableView>::New();
  this->activeSelectionTableView->SetFieldType(vtkQtTableView::EDGE_DATA);
  this->activeSelectionTableView->SetSelectionBehavior(vtkQtTableView::SELECT_ROWS);
  this->activeSelectionTableView->SetShowHorizontalHeaders(true);
  this->activeSelectionTableView->SetShowVerticalHeaders(false);
  this->activeSelectionTableView->SetEnableDragDrop(false);
  this->activeSelectionTableView->SetSortingEnabled(true);
  this->activeSelectionTableView->SetSplitMultiComponentColumns(false);

  this->selectedSubGraphExtraction->GetOutput(0)->Print(std::cout);
  this->activeSelectionTableView->SetRepresentationFromInputConnection(this->selectedSubGraphExtraction->GetOutputPort(0));
  this->activeSelectionTableRepresentation = this->activeSelectionTableView->GetRepresentation(0);

  this->dataAnimator->activeSelectionTableView = this->activeSelectionTableView;

  this->ui->selectedEdgesTable->layout()->addWidget(this->activeSelectionTableView->GetWidget());
  this->activeSelectionTableView->Update();
}

// Animation
void SpringNetworkTest::runAnimation()
{
  this->dataAnimationTimerId = this->cylindersRenderWindowInteractor->CreateRepeatingTimer(50);
  this->isAnimating = true;

  // Update GUI
  this->statusBar->showMessage(tr("Started animation"));
  this->ui->actionRunStopAnimation->setText((tr("Stop Animation")));
}

void SpringNetworkTest::stopAnimation()
{
  if (this->isAnimating) {
    if (this->cylindersRenderWindowInteractor->DestroyTimer(this->dataAnimationTimerId)) {
      this->isAnimating = false;

      // Update GUI
      this->statusBar->showMessage(tr("Stopped animation"));
      this->ui->actionRunStopAnimation->setText(tr("Run Animation"));
    } else {
      this->statusBar->showMessage(tr("Couldn't stop animation"));
    }
  } else {
    this->statusBar->showMessage(tr("Animation isn't running"));
  }
}

// SLOTS
// File
void SpringNetworkTest::close()
{
  std::cout << "Exiting" << std::endl;
  qApp->exit();
}

// Interaction
void SpringNetworkTest::setCylindersInteractorStyleToTrackballCamera()
{
  setActiveCylindersInteractorStyle(TRACKBALL_CAMERA);
}

void SpringNetworkTest::setCylindersInteractorStyleToUnicam()
{
  setActiveCylindersInteractorStyle(UNICAM);
}

void SpringNetworkTest::setCylindersInteractorStyleToEdgePicking()
{
  setActiveCylindersInteractorStyle(EDGE_PICKING);
}

// Animation
void SpringNetworkTest::runStopAnimation()
{
  if (this->isAnimating) {
    stopAnimation();
  } else {
    runAnimation();
  }
}

// Debug
void SpringNetworkTest::render()
{
  this->g->Modified();
  this->cylindersView->Render();
  this->edgesTableView->Update();
  this->statusBar->showMessage(tr("Rendered"));
}

void SpringNetworkTest::checkActiveSelection()
{
  vtkSelection* currentSelection = this->activeEdgeSelectionLink->GetAnnotationLayers()->GetCurrentSelection();
  std::cout << "active edge selection:" << std::endl;
  currentSelection->Print(std::cout);
  std::cout << std::endl;
  this->edgesTableView->Update();
}
