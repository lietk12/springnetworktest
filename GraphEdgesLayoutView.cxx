/*=========================================================================

  Program:   Visualization Toolkit
  Module:    GraphEdgesLayoutView.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*-------------------------------------------------------------------------
  Copyright 2008 Sandia Corporation.
  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
  the U.S. Government retains certain rights in this software.
-------------------------------------------------------------------------*/

#include "GraphEdgesLayoutView.h"

#include <vtkAlgorithmOutput.h>
#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtkDirectedGraph.h>
#include <vtkFast2DLayoutStrategy.h>
#include <vtkInteractorStyle.h>
#include <vtkObjectFactory.h>
#include "RenderedGraphEdgesRepresentation.h"
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSelection.h>
#include <vtkSimple2DLayoutStrategy.h>
#include <vtkTextProperty.h>

vtkStandardNewMacro(GraphEdgesLayoutView);
//----------------------------------------------------------------------------
GraphEdgesLayoutView::GraphEdgesLayoutView()
{
  this->SetInteractionModeTo2D();
  this->SetSelectionModeToFrustum();
  this->ReuseSingleRepresentationOn();
  this->VertexLabelsRequested = false;
  this->EdgeLabelsRequested = false;
  this->Interacting = false;
}

//----------------------------------------------------------------------------
GraphEdgesLayoutView::~GraphEdgesLayoutView()
{
}

//----------------------------------------------------------------------------
RenderedGraphEdgesRepresentation* GraphEdgesLayoutView::GetGraphRepresentation()
{
  RenderedGraphEdgesRepresentation* graphRep = 0;
  for (int i = 0; i < this->GetNumberOfRepresentations(); ++i)
    {
    vtkDataRepresentation* rep = this->GetRepresentation(i);
    graphRep = RenderedGraphEdgesRepresentation::SafeDownCast(rep);
    if (graphRep)
      {
      break;
      }
    }
  if (!graphRep)
    {
    vtkSmartPointer<vtkDirectedGraph> g = vtkSmartPointer<vtkDirectedGraph>::New();
    graphRep = RenderedGraphEdgesRepresentation::SafeDownCast(
      this->AddRepresentationFromInput(g));
    }
  return graphRep;
}

//----------------------------------------------------------------------------
vtkDataRepresentation* GraphEdgesLayoutView::CreateDefaultRepresentation(
  vtkAlgorithmOutput* port)
{
  RenderedGraphEdgesRepresentation* rep = RenderedGraphEdgesRepresentation::New();
  rep->SetInputConnection(port);
  return rep;
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::ProcessEvents(
  vtkObject* caller, unsigned long eventId, void* callData)
{
    if (eventId == vtkCommand::StartInteractionEvent)
    {
        if( GetHideVertexLabelsOnInteraction() && this->VertexLabelsRequested )
        {
            this->Interacting = true;
            this->GetGraphRepresentation()->SetVertexLabelVisibility(false);
        }
        if( GetHideEdgeLabelsOnInteraction() && this->EdgeLabelsRequested )
        {
            this->Interacting = true;
            this->GetGraphRepresentation()->SetEdgeLabelVisibility(false);
        }
    }
    if (eventId == vtkCommand::EndInteractionEvent)
    {
        bool forceRender = false ;
        if( GetHideVertexLabelsOnInteraction() && this->VertexLabelsRequested )
        {
            this->Interacting = false;
            this->GetGraphRepresentation()->SetVertexLabelVisibility(true);
            // Force the labels to reappear
            forceRender = true;
        }
        if( GetHideEdgeLabelsOnInteraction() && this->EdgeLabelsRequested )
        {
            this->Interacting = false;
            this->GetGraphRepresentation()->SetEdgeLabelVisibility(true);
            // Force the labels to reappear
            forceRender = true;
        }
        if(forceRender)
            // Force the labels to reappear
            this->Render();
    }
    if (eventId != vtkCommand::ComputeVisiblePropBoundsEvent)
    {
        this->Superclass::ProcessEvents(caller, eventId, callData);
    }
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetVertexLabelArrayName(const char* name)
{
  this->GetGraphRepresentation()->SetVertexLabelArrayName(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetVertexLabelArrayName()
{
  return this->GetGraphRepresentation()->GetVertexLabelArrayName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeLabelArrayName(const char* name)
{
  this->GetGraphRepresentation()->SetEdgeLabelArrayName(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetEdgeLabelArrayName()
{
  return this->GetGraphRepresentation()->GetEdgeLabelArrayName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetVertexLabelVisibility(bool vis)
{
    this->VertexLabelsRequested = vis ;
    // Don't update the visibility of the vertex label actor while an interaction
    // is in progress
    if(!this->Interacting)
        this->GetGraphRepresentation()->SetVertexLabelVisibility(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetVertexLabelVisibility()
{
  return this->GetGraphRepresentation()->GetVertexLabelVisibility();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetHideVertexLabelsOnInteraction(bool vis)
{
    this->GetGraphRepresentation()->SetHideVertexLabelsOnInteraction(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetHideVertexLabelsOnInteraction()
{
    return this->GetGraphRepresentation()->GetHideVertexLabelsOnInteraction();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetVertexVisibility(bool vis)
{
  this->GetGraphRepresentation()->SetVertexVisibility(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetVertexVisibility()
{
  return this->GetGraphRepresentation()->GetVertexVisibility();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeVisibility(bool vis)
{
  this->GetGraphRepresentation()->SetEdgeVisibility(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetEdgeVisibility()
{
  return this->GetGraphRepresentation()->GetEdgeVisibility();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeLabelVisibility(bool vis)
{
    this->EdgeLabelsRequested = vis ;
    // Don't update the visibility of the edge label actor while an interaction
    // is in progress
    if(!this->Interacting)
        this->GetGraphRepresentation()->SetEdgeLabelVisibility(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetEdgeLabelVisibility()
{
  return this->GetGraphRepresentation()->GetEdgeLabelVisibility();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetHideEdgeLabelsOnInteraction(bool vis)
{
    this->GetGraphRepresentation()->SetHideEdgeLabelsOnInteraction(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetHideEdgeLabelsOnInteraction()
{
    return this->GetGraphRepresentation()->GetHideEdgeLabelsOnInteraction();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetVertexColorArrayName(const char* name)
{
  this->GetGraphRepresentation()->SetVertexColorArrayName(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetVertexColorArrayName()
{
  return this->GetGraphRepresentation()->GetVertexColorArrayName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetColorVertices(bool vis)
{
  this->GetGraphRepresentation()->SetColorVerticesByArray(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetColorVertices()
{
  return this->GetGraphRepresentation()->GetColorVerticesByArray();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetVertexScalarBarVisibility(bool vis)
{
  this->GetGraphRepresentation()->SetVertexScalarBarVisibility(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetVertexScalarBarVisibility()
{
  return this->GetGraphRepresentation()->GetVertexScalarBarVisibility();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeColorArrayName(const char* name)
{
  this->GetGraphRepresentation()->SetEdgeColorArrayName(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetEdgeColorArrayName()
{
  return this->GetGraphRepresentation()->GetEdgeColorArrayName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetColorEdges(bool vis)
{
  this->GetGraphRepresentation()->SetColorEdgesByArray(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetColorEdges()
{
  return this->GetGraphRepresentation()->GetColorEdgesByArray();
}
//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeSelection(bool vis)
{
  this->GetGraphRepresentation()->SetEdgeSelection(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetEdgeSelection()
{
  return this->GetGraphRepresentation()->GetEdgeSelection();
}
//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeScalarBarVisibility(bool vis)
{
  this->GetGraphRepresentation()->SetEdgeScalarBarVisibility(vis);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetEdgeScalarBarVisibility()
{
  return this->GetGraphRepresentation()->GetEdgeScalarBarVisibility();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEnabledEdgesArrayName(const char* name)
{
  this->GetGraphRepresentation()->SetEnabledEdgesArrayName(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetEnabledEdgesArrayName()
{
  return this->GetGraphRepresentation()->GetEnabledEdgesArrayName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEnableEdgesByArray(bool vis)
{
  this->GetGraphRepresentation()->SetEnableEdgesByArray(vis);
}

//----------------------------------------------------------------------------
int GraphEdgesLayoutView::GetEnableEdgesByArray()
{
  return this->GetGraphRepresentation()->GetEnableEdgesByArray();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEnabledVerticesArrayName(const char* name)
{
  this->GetGraphRepresentation()->SetEnabledVerticesArrayName(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetEnabledVerticesArrayName()
{
  return this->GetGraphRepresentation()->GetEnabledVerticesArrayName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEnableVerticesByArray(bool vis)
{
  this->GetGraphRepresentation()->SetEnableVerticesByArray(vis);
}

//----------------------------------------------------------------------------
int GraphEdgesLayoutView::GetEnableVerticesByArray()
{
  return this->GetGraphRepresentation()->GetEnableVerticesByArray();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetGlyphType(int type)
{
  this->GetGraphRepresentation()->SetGlyphType(type);
}

//----------------------------------------------------------------------------
int GraphEdgesLayoutView::GetGlyphType()
{
  return this->GetGraphRepresentation()->GetGlyphType();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetScaledGlyphs(bool arg)
{
  this->GetGraphRepresentation()->SetScaling(arg);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetScaledGlyphs()
{
  return this->GetGraphRepresentation()->GetScaling();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetScalingArrayName(const char* name)
{
  this->GetGraphRepresentation()->SetScalingArrayName(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetScalingArrayName()
{
  return this->GetGraphRepresentation()->GetScalingArrayName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetIconArrayName(const char* name)
{
  this->GetGraphRepresentation()->SetVertexIconArrayName(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetIconArrayName()
{
  return this->GetGraphRepresentation()->GetVertexIconArrayName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::AddIconType(char *type, int index)
{
  this->GetGraphRepresentation()->AddVertexIconType(type, index);
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::ClearIconTypes()
{
  this->GetGraphRepresentation()->ClearVertexIconTypes();
}

//----------------------------------------------------------------------------
int GraphEdgesLayoutView::IsLayoutComplete()
{
  return this->GetGraphRepresentation()->IsLayoutComplete();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::UpdateLayout()
{
  this->GetGraphRepresentation()->UpdateLayout();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetLayoutStrategy(vtkGraphLayoutStrategy* s)
{
  this->GetGraphRepresentation()->SetLayoutStrategy(s);
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetLayoutStrategy(const char* name)
{
  this->GetGraphRepresentation()->SetLayoutStrategy(name);
}

//----------------------------------------------------------------------------
vtkGraphLayoutStrategy* GraphEdgesLayoutView::GetLayoutStrategy()
{
  return this->GetGraphRepresentation()->GetLayoutStrategy();
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetLayoutStrategyName()
{
  return this->GetGraphRepresentation()->GetLayoutStrategyName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeLayoutStrategy(vtkEdgeLayoutStrategy *s)
{
  this->GetGraphRepresentation()->SetEdgeLayoutStrategy(s);
}

//----------------------------------------------------------------------------
vtkEdgeLayoutStrategy* GraphEdgesLayoutView::GetEdgeLayoutStrategy()
{
  return this->GetGraphRepresentation()->GetEdgeLayoutStrategy();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeLayoutStrategy(const char* name)
{
  this->GetGraphRepresentation()->SetEdgeLayoutStrategy(name);
}

//----------------------------------------------------------------------------
const char* GraphEdgesLayoutView::GetEdgeLayoutStrategyName()
{
  return this->GetGraphRepresentation()->GetEdgeLayoutStrategyName();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetIconAlignment(int alignment)
{
  this->GetGraphRepresentation()->SetVertexIconAlignment(alignment);
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetIconVisibility(bool b)
{
  this->GetGraphRepresentation()->SetVertexIconVisibility(b);
}

//----------------------------------------------------------------------------
bool GraphEdgesLayoutView::GetIconVisibility()
{
  return this->GetGraphRepresentation()->GetVertexIconVisibility();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetVertexLabelFontSize(const int size)
{
  this->GetGraphRepresentation()->GetVertexLabelTextProperty()->SetFontSize(size);
}

//----------------------------------------------------------------------------
int GraphEdgesLayoutView::GetVertexLabelFontSize()
{
  return this->GetGraphRepresentation()->GetVertexLabelTextProperty()->GetFontSize();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::SetEdgeLabelFontSize(const int size)
{
  this->GetGraphRepresentation()->GetEdgeLabelTextProperty()->SetFontSize(size);
}

//----------------------------------------------------------------------------
int GraphEdgesLayoutView::GetEdgeLabelFontSize()
{
  return this->GetGraphRepresentation()->GetEdgeLabelTextProperty()->GetFontSize();
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::ZoomToSelection()
{
  double bounds[6];
  this->GetGraphRepresentation()->ComputeSelectedGraphBounds(bounds);
  this->Renderer->ResetCamera(bounds);
}

//----------------------------------------------------------------------------
void GraphEdgesLayoutView::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
